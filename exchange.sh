#!/bin/bash

# Check if a file name is provided as an argument
if [ $# -eq 0 ]; then
    echo "Usage: $0 <input_file>"
    exit 1
fi

# Set the path to the directory containing compiled .class files
class_path="target/classes"

# Run the Java application with the provided file name
java -cp "$class_path" org.bitvavo.ExchangeVerifier "$1"
