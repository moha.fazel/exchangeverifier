package org.bitvavo;

import org.bitvavo.model.Order;
import org.bitvavo.service.OrderBookService;
import org.bitvavo.utils.ExchangeUtil;

import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static org.bitvavo.utils.ExchangeUtil.readOrdersFromFile;

public class ExchangeVerifier {

    private static final Logger logger = Logger.getLogger(ExchangeUtil.class.getName());

    public static void main(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("Invalid input.");
        }

        // Read orders from file and create Order objects
        List<Order> orders = readOrdersFromFile(args[0]);

        if (!orders.isEmpty()) {
            // Create an instance of OrderBook
            OrderBookService service = new OrderBookService();

            // Populate the order book with orders
            for (Order order : orders) {
                service.addOrder(order);
            }

            // Match orders and get trade information
            service.matchOrders();

            // Generate and print order book output
            String output = service.generateOutput();
            System.out.println(output);
        } else {
            logger.log(SEVERE, "No order specified or they are not specified in the correct format.");
        }
    }
}