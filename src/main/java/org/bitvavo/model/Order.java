package org.bitvavo.model;

public class Order {
    private final String orderId;
    private final OrderType side;
    private final int price;

    private int quantity;

    public Order(String orderId, OrderType side, int price, int quantity) {
        this.orderId = orderId;
        this.side = side;
        this.price = price;
        this.quantity = quantity;
    }

    public String getOrderId() {
        return orderId;
    }

    public OrderType getSide() {
        return side;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
