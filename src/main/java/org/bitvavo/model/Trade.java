package org.bitvavo.model;

public record Trade(String aggressingOrderId, String restingOrderId, int price, int quantity) {
}