package org.bitvavo.model;

public enum OrderType {
    BUY('B'),
    SELL('S');

    public final char value;  // Additional data for each value

    // Constructor to associate data with each value
    OrderType(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }
}
