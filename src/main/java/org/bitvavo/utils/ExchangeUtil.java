package org.bitvavo.utils;

import org.bitvavo.model.Order;
import org.bitvavo.model.OrderType;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.Collections.emptyList;

public class ExchangeUtil {

    private static final Logger logger = Logger.getLogger(ExchangeUtil.class.getName());

    public static List<Order> readOrdersFromFile(String filename) throws IllegalArgumentException {
        List<Order> orders = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] orderData = line.split(",");
                if (orderData.length == 4) {
                    String orderId = orderData[0].trim();
                    char side = orderData[1].trim().charAt(0);
                    int price = Integer.parseInt(orderData[2].trim());
                    int quantity = Integer.parseInt(orderData[3].trim());

                    OrderType orderType = switch (side) {
                        case 'S' -> OrderType.SELL;
                        case 'B' -> OrderType.BUY;
                        default -> {
                            logger.log(Level.SEVERE, "Invalid order type: " + side);
                            throw new IllegalArgumentException();
                        }
                    };

                    Order order = new Order(orderId, orderType, price, quantity);
                    validateOrder(order);
                    orders.add(order);
                } else {
                    logger.log(Level.SEVERE, "Invalid order format: " + line);
                    throw new IllegalArgumentException();
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "I/O exception");
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            if (e instanceof NumberFormatException) {
                logger.log(Level.SEVERE, "The numbers for price and quantity should be positive Integer values.");
            }
            orders = emptyList();
        }
        return orders;
    }

    private static void validateOrder(Order order) {
        if (order.getOrderId().isEmpty()) {
            logger.log(Level.SEVERE, "Id is not specified for the order");
            throw new IllegalArgumentException();
        }

        if (order.getPrice() <= 0) {
            logger.log(Level.SEVERE, "Invalid price: " + order.getPrice());
            throw new IllegalArgumentException();
        }

        if (order.getQuantity() <= 0) {
            logger.log(Level.SEVERE, "Invalid quantity: " + order.getQuantity());
            throw new IllegalArgumentException();
        }
    }
}
