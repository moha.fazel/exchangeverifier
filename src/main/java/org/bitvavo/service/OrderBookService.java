package org.bitvavo.service;

import org.bitvavo.model.Order;
import org.bitvavo.model.Trade;

import java.util.*;

import static org.bitvavo.model.OrderType.BUY;

public class OrderBookService {

    final TreeMap<Integer, List<Order>> buyOrders = new TreeMap<>(Collections.reverseOrder());
    final TreeMap<Integer, List<Order>> sellOrders = new TreeMap<>();
    final List<Trade> trades = new ArrayList<>();

    public void addOrder(Order order) {
        TreeMap<Integer, List<Order>> ordersMap;

        if (order.getSide() == BUY) {
            ordersMap = buyOrders;
        } else {
            ordersMap = sellOrders;
        }

        ordersMap.compute(order.getPrice(), (price, orders) -> {
            if (orders == null) {
                orders = new ArrayList<>();
            }
            orders.add(order);
            return orders;
        });
    }

    public void matchOrders() {
        for (Map.Entry<Integer, List<Order>> buyEntry : buyOrders.entrySet()) {
            int buyPrice = buyEntry.getKey();
            List<Order> buyOrdersAtPrice = buyEntry.getValue();

            // Match against sell orders at lower price levels
            NavigableMap<Integer, List<Order>> lowerSellOrders = sellOrders.headMap(buyPrice, true);
            for (Map.Entry<Integer, List<Order>> lowerEntry : lowerSellOrders.entrySet()) {
                int lowerPrice = lowerEntry.getKey();
                List<Order> lowerSellOrdersAtPrice = lowerEntry.getValue();
                matchOrdersAtPrice(trades, buyOrdersAtPrice, lowerSellOrdersAtPrice, lowerPrice);
            }

            // If the top buy price is less than or equal to the top sell price, stop iterating
            if (!sellOrders.isEmpty() && buyPrice <= sellOrders.firstKey()) {
                break;
            }
        }
    }

    private void matchOrdersAtPrice(List<Trade> trades, List<Order> buyOrders, List<Order> sellOrders, int price) {
        for (Iterator<Order> buyIterator = buyOrders.iterator(); buyIterator.hasNext();) {
            Order buyOrder = buyIterator.next();

            for (Iterator<Order> sellIterator = sellOrders.iterator(); sellIterator.hasNext();) {
                Order sellOrder = sellIterator.next();

                if (buyOrder.getQuantity() == sellOrder.getQuantity()) {
                    trades.add(new Trade(buyOrder.getOrderId(), sellOrder.getOrderId(),
                            price, buyOrder.getQuantity()));
                    buyIterator.remove();
                    sellIterator.remove();
                    break;
                } else if (buyOrder.getQuantity() < sellOrder.getQuantity()) {
                    trades.add(new Trade(buyOrder.getOrderId(), sellOrder.getOrderId(),
                            price, buyOrder.getQuantity()));
                    sellOrder.setQuantity(sellOrder.getQuantity() - buyOrder.getQuantity());
                    buyIterator.remove();
                    break;
                } else {
                    trades.add(new Trade(buyOrder.getOrderId(), sellOrder.getOrderId(),
                            price, sellOrder.getQuantity()));
                    buyOrder.setQuantity(buyOrder.getQuantity() - sellOrder.getQuantity());
                    sellIterator.remove();
                }
            }
        }
    }

    public String generateOutput() {
        StringBuilder outputBuilder = new StringBuilder();

        for (Trade trade : trades) {
            outputBuilder.append(String.format("trade %s,%s,%d,%d\n",
                    trade.aggressingOrderId(), trade.restingOrderId(),
                    trade.price(), trade.quantity()));
        }

        Iterator<Order> buyIterator = getIterator(buyOrders);
        Iterator<Order> sellIterator = getIterator(sellOrders);

        while (buyIterator.hasNext() || sellIterator.hasNext()) {
            if (buyIterator.hasNext()) {
                Order buyOrder = buyIterator.next();
                outputBuilder.append(String.format("%,11d %6d |", buyOrder.getQuantity(), buyOrder.getPrice()).replace(".", ","));
            } else {
                outputBuilder.append(String.format("%11s %6s |", "", ""));
            }

            if (sellIterator.hasNext()) {
                Order sellOrder = sellIterator.next();
                outputBuilder.append(String.format(" %6d %,11d\n", sellOrder.getPrice(), sellOrder.getQuantity()).replace(".", ","));
            } else {
                outputBuilder.append(String.format("%6s %11s \n", "", ""));
            }
        }

        return outputBuilder.toString();
    }

    private static Iterator<Order> getIterator(NavigableMap<Integer, List<Order>> orders) {
        List<Order> allOrders = new ArrayList<>();
        for (List<Order> orderList : orders.values()) {
            allOrders.addAll(orderList);
        }
        return allOrders.iterator();
    }

}
