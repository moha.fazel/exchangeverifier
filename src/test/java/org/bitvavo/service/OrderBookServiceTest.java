package org.bitvavo.service;

import org.bitvavo.model.Order;
import org.bitvavo.utils.ExchangeUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.bitvavo.constants.TestConstants.*;
import static org.bitvavo.utils.TestUtil.calculateMD5;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class OrderBookServiceTest {

    private List<Order> orders;
    private OrderBookService service;

    private void initializeOrders(String inputPath) {
        service = new OrderBookService();
        orders = ExchangeUtil.readOrdersFromFile(inputPath);
        for (Order order : orders) {
            service.addOrder(order);
        }
    }

    @Test
    void addOrder_whenValidInput_returnOrders() {
        initializeOrders(VALID_INPUT_NO_TRADE_PATH);
        assertNotNull(service.buyOrders);
        assertNotNull(service.sellOrders);
        assertEquals(2, service.buyOrders.size());
        assertEquals(3, service.sellOrders.size());
        assertEquals(1, service.buyOrders.get(99).size());
        assertEquals(1, service.buyOrders.get(98).size());
        assertEquals(2, service.sellOrders.get(100).size());
        assertEquals(1, service.sellOrders.get(103).size());
        assertEquals(1, service.sellOrders.get(105).size());
        assertEquals("10001", service.sellOrders.get(100).get(0).getOrderId());
        assertEquals("10002", service.sellOrders.get(100).get(1).getOrderId());
    }

    @Test
    void addOrder_whenNoBuyOrder_returnEmptyBuyOrderMap() {
        initializeOrders(VALID_INPUT_SELL_ORDERS_PATH);
        assertNotNull(service.buyOrders);
        assertNotNull(service.sellOrders);
        assertEquals(0, service.buyOrders.size());
        assertEquals(2, service.sellOrders.size());
        assertEquals(2, service.sellOrders.get(99).size());
        assertEquals(1, service.sellOrders.get(98).size());
        assertEquals("10007", service.sellOrders.get(99).get(0).getOrderId());
        assertEquals("10001", service.sellOrders.get(99).get(1).getOrderId());
    }

    @Test
    void addOrder_whenNoSellOrder_returnEmptySellOrderMap() {
        initializeOrders(VALID_INPUT_BUY_ORDERS_PATH);
        assertNotNull(service.buyOrders);
        assertNotNull(service.sellOrders);
        assertEquals(2, service.buyOrders.size());
        assertEquals(0, service.sellOrders.size());
        assertEquals(2, service.buyOrders.get(99).size());
        assertEquals(1, service.buyOrders.get(98).size());
        assertEquals("10000", service.buyOrders.get(99).get(0).getOrderId());
        assertEquals("10007", service.buyOrders.get(99).get(1).getOrderId());
    }

    @Test
    void matchOrders_whenValidInputNoTrades_returnNoTrades() {
        initializeOrders(VALID_INPUT_NO_TRADE_PATH);
        service.matchOrders();
        assertNotNull(service.trades);
        assertEquals(0, service.trades.size());
    }

    @Test
    void matchOrders_whenValidInputPotentialTrades_returnTrades() {
        initializeOrders(VALID_INPUT_POTENTIAL_TRADES_PATH);
        service.matchOrders();
        assertNotNull(service.trades);
        assertEquals(4, service.trades.size());
        assertEquals("10006", service.trades.get(0).aggressingOrderId());
        assertEquals("10001", service.trades.get(0).restingOrderId());
        assertEquals(100, service.trades.get(0).price());
        assertEquals(500, service.trades.get(0).quantity());
        assertEquals("10006", service.trades.get(1).aggressingOrderId());
        assertEquals("10002", service.trades.get(1).restingOrderId());
        assertEquals(100, service.trades.get(1).price());
        assertEquals(10000, service.trades.get(1).quantity());
        assertEquals("10006", service.trades.get(2).aggressingOrderId());
        assertEquals("10004", service.trades.get(2).restingOrderId());
        assertEquals(103, service.trades.get(2).price());
        assertEquals(100, service.trades.get(2).quantity());
        assertEquals("10006", service.trades.get(3).aggressingOrderId());
        assertEquals("10005", service.trades.get(3).restingOrderId());
        assertEquals(105, service.trades.get(3).price());
        assertEquals(5400, service.trades.get(3).quantity());
        assertEquals(3, service.buyOrders.size());
        assertEquals(0, service.buyOrders.get(105).size());
        assertEquals(1, service.buyOrders.get(99).size());
        assertEquals(1, service.buyOrders.get(98).size());
        assertEquals(3, service.sellOrders.size());
        assertEquals(0, service.sellOrders.get(100).size());
        assertEquals(0, service.sellOrders.get(103).size());
        assertEquals(1, service.sellOrders.get(105).size());
        assertEquals(14600, service.sellOrders.get(105).get(0).getQuantity());
    }

    @ParameterizedTest
    @MethodSource
    void generateOutput_and_CheckMD5(String inputPath, String expectedMD5) {
        initializeOrders(inputPath);
        service.matchOrders();
        assertEquals(expectedMD5, calculateMD5(service.generateOutput()));
    }

    private static Stream<Arguments> generateOutput_and_CheckMD5() {
        return Stream.of(
                Arguments.of(VALID_INPUT_NO_TRADE_PATH, VALID_INPUT_NO_TRADE_MD5),
                Arguments.of(VALID_INPUT_POTENTIAL_TRADES_PATH, VALID_INPUT_POTENTIAL_TRADE_MD5)
        );
    }
}