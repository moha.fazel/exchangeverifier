package org.bitvavo.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TestUtil {

    public static String calculateMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] bytes = md.digest(input.getBytes());

            StringBuilder hashBuilder = new StringBuilder();
            for (byte b : bytes) {
                hashBuilder.append(String.format("%02x", b));
            }

            return hashBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error calculating MD5 hash", e);
        }
    }
}
