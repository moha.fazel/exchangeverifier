package org.bitvavo.utils;

import org.bitvavo.model.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.bitvavo.constants.TestConstants.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ExchangeUtilTest {

    private List<Order> orders;

    @Test
    void readOrdersFromFile_happyFlow_returnOrders() {
        orders = ExchangeUtil.readOrdersFromFile(VALID_INPUT_NO_TRADE_PATH);
        assertEquals(6, orders.size());
    }

    @ParameterizedTest
    @MethodSource
    void readOrdersFromFile_invalidInput_notThrowException(String inputPath) {
        assertDoesNotThrow(() -> ExchangeUtil.readOrdersFromFile(inputPath));
    }

    private static Stream<Arguments> readOrdersFromFile_invalidInput_notThrowException() {
        return Stream.of(
                Arguments.of(FILE_NOT_EXIST_PATH),
                Arguments.of(EMPTY_FILE_PATH),
                Arguments.of(INVALID_INPUT_PATH),
                Arguments.of(INVALID_ID_PATH),
                Arguments.of(INVALID_SIDE_PATH),
                Arguments.of(INVALID_PRICE_PATH),
                Arguments.of(INVALID_QUANTITY_PATH),
                Arguments.of(INVALID_PRICE_DOUBLE_PATH),
                Arguments.of(INVALID_Quantity_DOUBLE_PATH)
        );
    }

}