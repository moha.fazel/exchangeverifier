package org.bitvavo.constants;

public class TestConstants {

    public static final String VALID_INPUT_NO_TRADE_PATH = "src/test/resources/valid_input_no_trade.txt";
    public static final String VALID_INPUT_POTENTIAL_TRADES_PATH = "src/test/resources/valid_input_potential_trades.txt";
    public static final String VALID_INPUT_BUY_ORDERS_PATH = "src/test/resources/valid_input_buy_orders.txt";
    public static final String VALID_INPUT_SELL_ORDERS_PATH = "src/test/resources/valid_input_sell_orders.txt";
    public static final String FILE_NOT_EXIST_PATH = "src/test/resources/inputFileNotExist.txt";
    public static final String EMPTY_FILE_PATH = "src/test/resources/empty.txt";
    public static final String INVALID_INPUT_PATH = "src/test/resources/input_invalid_input.txt";
    public static final String INVALID_ID_PATH = "src/test/resources/input_invalid_id.txt";
    public static final String INVALID_SIDE_PATH = "src/test/resources/input_invalid_side.txt";
    public static final String INVALID_PRICE_PATH = "src/test/resources/input_invalid_price.txt";
    public static final String INVALID_PRICE_DOUBLE_PATH = "src/test/resources/input_invalid_double_price.txt";
    public static final String INVALID_Quantity_DOUBLE_PATH = "src/test/resources/input_invalid_double_quantity.txt";
    public static final String INVALID_QUANTITY_PATH = "src/test/resources/input_invalid_quantity.txt";
    public static final String VALID_INPUT_NO_TRADE_MD5 = "8ff13aad3e61429bfb5ce0857e846567";
    public static final String VALID_INPUT_POTENTIAL_TRADE_MD5 = "ce8e7e5ab26ab5a7db6b7d30759cf02e";
}
