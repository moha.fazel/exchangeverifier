# Exchange Verifier Java Service

The Exchange Verifier Java Service is a program designed to verify the behavior of a trading exchange by comparing its output with expected results. The program takes input orders from a text file, matches the trades, and produces output that can be compared with expected results.

## Getting Started

Follow the steps below to set up and run the Exchange Verifier Java Service:

### Prerequisites

- Java Development Kit (JDK) 17 or later
- Apache Maven

### Installation

1. Clone or download this repository to your local machine.

2. Navigate to the project root directory.

### Usage

1. Compile the Java code using Maven:

   ```bash
   mvn clean compile
   ```
2. Make the shell script file executable by running the following command:

   ```bash
   chmod +x exchange.sh
   ```
3. Run the Java service using the provided shell script:

   ```bash
   ./exchange.sh <input_file>
   ```
   There are sample test files in test directory under resources folder that you can use for test. You can do that by running the provided shell script.
   ```bash
   ./exchange.sh src/test/resources/valid_input_potential_trades.txt
   ```
### Output
The program will generate output that represents the current state of the order book after processing the input orders. It will also include the MD5 hash of the output, which can be compared with expected MD5 values provided in the task.

## Configuration
The run.sh script is used to run the Java program. You can modify the classpath and other parameters in this script.

## Contributing
Contributions are welcome! If you find any issues or have improvements to suggest, please feel free to submit a pull request.